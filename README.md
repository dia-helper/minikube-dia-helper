Для запуска minikube:
0. ```
    minikube config set cpus 6
    minikube config set memory 16384
    minikube delete
    minikube start
    ```
1. ```minikube mount /var/www:/var/www```
2. ```minikube ip``` >> ```sudo vim /etc/hosts``` >> ```192.168.49.2 api.diakube.com```
3. ```minikube addons enable ingress```
4. ```
    kubectl delete all --all
    kubectl apply -f .helm/
   ```
   
Команды для работы:

```kubectl get pods```

```kubectl exec -it dia-helper-deployment-65d9bc66bc-4rhcj --container php-fpm -- /bin/sh```

```minikube dashboard```