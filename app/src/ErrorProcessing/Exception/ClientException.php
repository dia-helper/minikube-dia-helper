<?php

declare(strict_types=1);

namespace App\ErrorProcessing\Exception;

use InvalidArgumentException;

abstract class ClientException extends InvalidArgumentException
{
}
