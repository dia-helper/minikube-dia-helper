<?php

declare(strict_types=1);

namespace App\ErrorProcessing\EventSubscriber;

use App\ErrorProcessing\Exception\ClientException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'processException',
        ];
    }

    public function processException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse();
        if ($exception instanceof ClientException) {
            $response->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            $response->setJson(json_encode($exception->getMessage()) ?: 'client error');
        } else {
            $response->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            $response->setJson(json_encode('server error') ?: 'server error');
        }

        $event->setResponse($response);
    }
}
