<?php

declare(strict_types=1);

namespace App\Open\Dto;

final class ChartDataDto
{
    /**
     * @var int[]
     */
    private array $labels;

    /**
     * @var float[]
     */
    private array $data;

    /**
     * @param int[]   $labels
     * @param float[] $data
     */
    public function __construct(array $labels, array $data)
    {
        $this->labels = $labels;
        $this->data = $data;
    }

    /**
     * @return int[]
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @return float[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
