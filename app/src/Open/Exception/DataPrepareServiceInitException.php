<?php

declare(strict_types=1);

namespace App\Open\Exception;

use App\ErrorProcessing\Exception\ClientException;

class DataPrepareServiceInitException extends ClientException
{
}
