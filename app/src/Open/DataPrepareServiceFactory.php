<?php

declare(strict_types=1);

namespace App\Open;

use App\Open\Exception\DataPrepareServiceInitException;
use App\Open\Service\ArchiverInterface;
use App\Open\Service\GlimpCsvDataPreparer;
use App\Open\Service\OneTouchRevealCsvDataPreparer;
use App\Open\Service\XdrippCsvDataPreparer;
use App\Open\Service\XdrippSqliteDataPreparer;
use Symfony\Component\HttpFoundation\RequestStack;

final class DataPrepareServiceFactory
{
    private const TYPE_ONE_TOUCH_REVEAL_CSV = 'one-touch-reveal-csv';
    private const XDRIPP_CSV = 'xdripp-csv';
    private const XDRIPP_SQLITE = 'xdripp-sqlite';
    private const GLIMP_CSV = 'glimp-csv';

    private RequestStack $requestStack;

    private ArchiverInterface $archiver;

    public function __construct(RequestStack $requestStack, ArchiverInterface $archiver)
    {
        $this->requestStack = $requestStack;
        $this->archiver = $archiver;
    }

    public function __invoke(): DataPreparerInterface
    {
        $request = $this->requestStack->getCurrentRequest();
        $serviceType = $request->get('serviceType');

        switch ($serviceType) {
            case self::TYPE_ONE_TOUCH_REVEAL_CSV:
                $service = new OneTouchRevealCsvDataPreparer();

                break;
            case self::XDRIPP_CSV:
                $service = new XdrippCsvDataPreparer($this->archiver);

                break;
            case self::XDRIPP_SQLITE:
                $service = new XdrippSqliteDataPreparer($this->archiver);

                break;
            case self::GLIMP_CSV:
                $service = new GlimpCsvDataPreparer();

                break;
            default:
                throw new DataPrepareServiceInitException('Unresolved service type');
        }

        return $service;
    }
}
