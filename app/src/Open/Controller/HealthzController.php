<?php

declare(strict_types=1);

namespace App\Open\Controller;

use App\Open\DataPreparerInterface;
use App\Open\Exception\FileExtractException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class HealthzController extends AbstractController
{
    /**
     * @Route(path="/open/healthz", methods={"GET"})
     */
    public function __invoke(UploadedFile $file = null): JsonResponse
    {
        return $this->json(['status' => 'ok']);
    }
}
