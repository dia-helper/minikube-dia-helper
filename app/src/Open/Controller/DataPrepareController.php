<?php

declare(strict_types=1);

namespace App\Open\Controller;

use App\Open\DataPreparerInterface;
use App\Open\Exception\FileExtractException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class DataPrepareController extends AbstractController
{
    private const MAX_FILE_SIZE = '10M';
    private DataPreparerInterface $dataPreparer;

    private ValidatorInterface $validator;

    public function __construct(DataPreparerInterface $dataPreparer, ValidatorInterface $validator)
    {
        $this->dataPreparer = $dataPreparer;
        $this->validator = $validator;
    }

    /**
     * @Route(path="/open/build-chart", methods={"POST"})
     */
    public function __invoke(UploadedFile $file = null): JsonResponse
    {
        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($file, [
            new File(['maxSize' => self::MAX_FILE_SIZE, 'mimeTypes' => $this->dataPreparer->getAcceptableMimeType()]),
            new NotBlank(),
        ]);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return $this->json($errorsString, JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }

        try {
            $data = $this->dataPreparer->prepareData($file);
        } catch (FileExtractException $e) {
            return $this->json($e->getMessage(), JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }

        return $this->json($data);
    }
}
