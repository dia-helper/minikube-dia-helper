<?php

declare(strict_types=1);

namespace App\Open\Service;

use App\Open\DataPreparerInterface;
use App\Open\Dto\ChartDataDto;
use App\Open\Exception\DataStructureException;
use App\Open\Exception\FileExtractException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class GlimpCsvDataPreparer implements DataPreparerInterface
{
    private const ACCEPTABLE_MIME_TYPE_LIST = ['text/plain', 'text/csv', 'application/octet-stream', 'application/gzip'];

    public function prepareData(UploadedFile $file): ChartDataDto
    {
        $data = [];
        $filePath = $file->getRealPath();
        if (!$filePath) {
            throw new FileExtractException('file path extract error');
        }
        if (($handle = gzopen($filePath, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                if (count($row) < 6) {
                    continue;
                }
                if (!empty($rawVal = self::decode($row[6]))) {
                    $value = $rawVal;
                } elseif (!empty($rawVal = self::decode($row[5]))) {
                    $value = $rawVal;
                } else {
                    continue;
                }

                $dateTime = \DateTimeImmutable::createFromFormat('d/m/Y H.i.s', self::decode($row[1]));
                if (!$dateTime) {
                    throw new DataStructureException('invalid date format');
                }
                $label = $dateTime->getTimestamp() * 1000;
                $data[$label] = (float) $value;
            }
            fclose($handle);
        }
        ksort($data);

        return new ChartDataDto(array_keys($data), array_values($data));
    }

    public function getAcceptableMimeType(): array
    {
        return self::ACCEPTABLE_MIME_TYPE_LIST;
    }

    private static function decode(string $string): string
    {
        return mb_convert_encoding($string, 'UTF-8', 'UTF-16');
    }
}
