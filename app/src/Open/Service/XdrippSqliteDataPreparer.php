<?php

declare(strict_types=1);

namespace App\Open\Service;

use App\Open\DataPreparerInterface;
use App\Open\Dto\ChartDataDto;
use App\Open\Exception\DataStructureException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class XdrippSqliteDataPreparer implements DataPreparerInterface
{
    private const ACCEPTABLE_MIME_TYPE_LIST = ['application/zip'];

    private ArchiverInterface $archiver;

    public function __construct(ArchiverInterface $archiver)
    {
        $this->archiver = $archiver;
    }

    public function prepareData(UploadedFile $file): ChartDataDto
    {
        $unzippedFilePath = $this->archiver->getUnzippedFilePath($file);
        $db = new \SQLite3($unzippedFilePath, SQLITE3_OPEN_READONLY);
        $queryString = 'SELECT filtered_calculated_value, timestamp FROM main.BgReadings ORDER BY timestamp ASC';
        $query = $db->query($queryString);
        $data = [];
        $labels = [];
        if (!$query) {
            throw new DataStructureException('incorrect data structure');
        }
        while ($row = $query->fetchArray()) {
            $value = (float) $row['filtered_calculated_value'];
            if (empty($value) || $value < 1) {
                continue;
            }
            $labels[] = $row['timestamp'];
            $data[] = $value;
        }
        unlink($unzippedFilePath);

        return new ChartDataDto($labels, $data);
    }

    public function getAcceptableMimeType(): array
    {
        return self::ACCEPTABLE_MIME_TYPE_LIST;
    }
}
