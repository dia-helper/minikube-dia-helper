<?php

namespace App\Open\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface ArchiverInterface
{
    public function getUnzippedFilePath(UploadedFile $file): string;
}
