<?php

declare(strict_types=1);

namespace App\Open\Service;

use App\Open\DataPreparerInterface;
use App\Open\Dto\ChartDataDto;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class XdrippCsvDataPreparer implements DataPreparerInterface
{
    private const ACCEPTABLE_MIME_TYPE_LIST = ['application/zip'];

    private ArchiverInterface $archiver;

    public function __construct(ArchiverInterface $archiver)
    {
        $this->archiver = $archiver;
    }

    public function prepareData(UploadedFile $file): ChartDataDto
    {
        $data = [];
        $labels = [];
        $unzippedFilePath = $this->archiver->getUnzippedFilePath($file);
        if (($handle = fopen($unzippedFilePath, 'r')) !== false) {
            $i = 0;
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                ++$i;
                if (1 == $i || empty($row[2])) {
                    continue;
                }
                $dateTimeString = $row[0] . ' ' . $row[1];
                $dateTime = new \DateTimeImmutable($dateTimeString);
                $labels[] = $dateTime->getTimestamp() * 1000;
                $data[] = (float) $row[2];
            }
            fclose($handle);
        }
        unlink($unzippedFilePath);

        return new ChartDataDto($labels, $data);
    }

    public function getAcceptableMimeType(): array
    {
        return self::ACCEPTABLE_MIME_TYPE_LIST;
    }
}
