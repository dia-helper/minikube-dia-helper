<?php

declare(strict_types=1);

namespace App\Open;

use App\Open\Dto\ChartDataDto;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface DataPreparerInterface
{
    public function prepareData(UploadedFile $file): ChartDataDto;

    /**
     * @return string[]
     */
    public function getAcceptableMimeType(): array;
}
